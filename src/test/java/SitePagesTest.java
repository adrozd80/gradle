import org.junit.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SitePagesTest {
    private static WebDriver driver;
    private static LoginPage loginPage;
    private static MainPage mainPage;
    private static SignUpPage signUpPage;
    private static String userName;
    private static String password;
    private static String email;
    private static String name;
    private static String address;
    private static String aboutText;
    private int randNumber;
    private static String webDriverPath = "C:\\Users\\admin\\IdeaProjects\\Gradle\\drivers\\chromedriver.exe";
    private static String url = "https://kinogo.by/";

    public static int getRandomNumber(int min, int max) {
        return (int) (Math.floor(Math.random() * (max - min + 1)) + min);
    }

    @BeforeClass
    public static void createUserForTests() {
        int randNumber1 = getRandomNumber(3333, 4444);
        int randNumber2 = getRandomNumber(5555, 7777);
        userName = randNumber1 + "_qa_test_user_" + randNumber2;
        password = "qa_test_pass";
        email = userName + "@test.com";
        name = "QA_TEST_USER";
        address = "QA City";
        aboutText = "QA description";

        System.setProperty("webdriver.chrome.driver", webDriverPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(url);
        loginPage = new LoginPage(driver);

        signUpPage = loginPage.clickRegistrationLink();
        mainPage = signUpPage.userCorrectRegistration(userName, password, email, name, address, aboutText);
        mainPage.clickExitLink();

        driver.quit();
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", webDriverPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(url);
        loginPage = new LoginPage(driver);
        randNumber = getRandomNumber(200, 500);
    }

    @Test
//    @Ignore
    public void checkExistLoginLink() {
        Assert.assertEquals(true, loginPage.checkLoginLink());
    }

    @Test
//    @Ignore
    public void checkCorrectLogin() {
        MainPage mainPage = loginPage.loginToSite(userName, password);
        Assert.assertEquals(userName, mainPage.getProfileLinkText(userName));
    }

    @Test
//    @Ignore
    public void checkExistExitLink() {
        MainPage mainPage = loginPage.loginToSite(userName, password);
        Assert.assertTrue(mainPage.checkExitLink());
    }

    @Test
//    @Ignore
    public void checkSuccessfullyLogout() {
        MainPage mainPage = loginPage.loginToSite(userName, password);
        mainPage.clickExitLink();
        Assert.assertEquals(true, loginPage.checkLoginLink());
    }

//    Start Negative Tests

    @Test
//    @Ignore
    public void checkInvalidLogin() {
        loginPage.failLoginToSite(loginPage.invalidUser + randNumber, loginPage.invalidPassword + randNumber);
        Assert.assertTrue(loginPage.checkErrorLoginTextExist());
    }

    @Test
//    @Ignore
    public void checkInvalidUserName() {
        loginPage.failLoginToSite(loginPage.invalidUser + randNumber, password);
        Assert.assertTrue(loginPage.checkErrorLoginTextExist());
    }

    @Test
//    @Ignore
    public void checkInvalidPassword() {
        loginPage.failLoginToSite(userName, loginPage.invalidPassword + randNumber);
        Assert.assertTrue(loginPage.checkErrorLoginTextExist());
    }

    @Test
//    @Ignore
    public void checkEmptyLogin() {
        loginPage.failLoginToSite("", "");
        Assert.assertTrue(loginPage.checkErrorLoginTextExist());
    }

//    Stop Negative Tests

    @Test
//    @Ignore
    public void checkNickNameAndStatus() {
        MainPage mainPage = loginPage.loginToSite(userName, password);
        ProfilePage profilePage = mainPage.clickProfileLink(userName);
        Assert.assertEquals(userName, profilePage.getNickName());
        Assert.assertEquals("Online", profilePage.getStatus());
    }

    @Test
//    @Ignore
    public void existMovieNameOnPage() {
        MainPage mainPage = loginPage.loginToSite(userName, password);
        Assert.assertTrue(mainPage.existMovieNameOnPage("Test") == false);
    }

    @Test
//    @Ignore
    public void getFourthMovieFromTopPremierePage() {
        MainPage mainPage = loginPage.loginToSite(userName, password);
        mainPage.clickPremiereLink();
        List<String> films = mainPage.selectAllFilmsFromPaginationPage();
        System.out.println(films.get(3));
        Assert.assertTrue(true);
    }

    @Test
//    @Ignore
    public void checkExistListFilmsOnPage() {
        MainPage mainPage = loginPage.loginToSite(userName, password);
        mainPage.clickPremiereLink();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollTo(0, 5000) ");
        mainPage.selectPaginationPageByNumber("4");
        System.out.println(mainPage.selectAllFilmsFromPaginationPage().size());
        System.out.println(mainPage.selectAllFilmsFromPaginationPage());
        Assert.assertEquals(12, mainPage.selectAllFilmsFromPaginationPage().size());
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @AfterClass
    public static void finish() {
        System.out.println("Finish Tests");
    }

}
