import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProfilePage {
    private WebDriver driver;

    public ProfilePage(WebDriver driver) {
        this.driver = driver;
    }

    private By nickName = By.xpath("//div[@class='padding_border']//b[2]");
    private By status = By.xpath("//div[@class='padding_border']//span[1]");

    public String getNickName() {
        return driver.findElement(nickName).getText();
    }

    public String getStatus() {
        return driver.findElement(status).getText();
    }

}
