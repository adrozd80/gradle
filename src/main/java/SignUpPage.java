import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignUpPage {
    private WebDriver driver;

    public SignUpPage(WebDriver driver) {
        this.driver = driver;
    }

    private By loginField = By.xpath("//input[@name='name']");
    private By passwordField = By.xpath("//input[@name='password1']");
    private By repeatPasswordField = By.xpath("//input[@name='password2']");
    private By emailField = By.xpath("//input[@name='email']");
    private By sendButton = By.xpath("//div[@class='fieldsubmit']/button");
    private By nameField = By.xpath("//input[@name='fullname']");
    private By addressField = By.xpath("//input[@name='land']");
    private By aboutField = By.xpath("//textarea[@name='info']");


    public SignUpPage fillLoginNameField(String userName) {
        driver.findElement(loginField).sendKeys(userName);
        return this;
    }

    public SignUpPage fillPasswordField(String password) {
        driver.findElement(passwordField).sendKeys(password);
        return this;
    }

    public SignUpPage fillRepeatPasswordField(String password) {
        driver.findElement(repeatPasswordField).sendKeys(password);
        return this;
    }

    public SignUpPage fillEmailField(String email) {
        driver.findElement(emailField).sendKeys(email);
        return this;
    }

    public SignUpPage clickSendButton() {
        driver.findElement(sendButton).click();
        return this;
    }

    public SignUpPage fillNameField(String name) {
        driver.findElement(nameField).sendKeys(name);
        return this;
    }

    public SignUpPage fillAddressField(String address) {
        driver.findElement(addressField).sendKeys(address);
        return this;
    }

    public SignUpPage fillAboutField(String aboutText) {
        driver.findElement(aboutField).sendKeys(aboutText);
        return this;
    }

    public void userRegistration(String userName, String password, String email) {
        this.fillLoginNameField(userName);
        this.fillPasswordField(password);
        this.fillRepeatPasswordField(password);
        this.fillEmailField(email);
        this.clickSendButton();
    }

    public void updateUserProfile(String name, String address, String aboutText) {
        this.fillNameField(name);
        this.fillAddressField(address);
        this.fillAboutField(aboutText);
        this.clickSendButton();
    }

    public MainPage userCorrectRegistration(String userName, String password, String email, String name, String address, String aboutText) {
        this.userRegistration(userName, password, email);
        this.updateUserProfile(name, address, aboutText);
        return new MainPage(driver);
    }

}
