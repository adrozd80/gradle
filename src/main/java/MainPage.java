import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    private By exitLink = By.xpath("//div[@class='loginin']//a[contains(@href, 'logout')]");
    private By premiereLink = By.xpath("//a[@href='/film/premiere/']");
    private By allFilmsFromPage = By.xpath("//h2[@class='zagolovki']//a");
    private String profileLinkTest = "//div[@class='loginin']//a[text()='%s']";
    private List<WebElement> films = new ArrayList<>();

    public String getProfileLinkText(String userName) {
        return driver.findElement(By.xpath(String.format(profileLinkTest, userName))).getText();
    }

    public MainPage clickPremiereLink() {
        driver.findElement(premiereLink).click();
        return this;
    }

    public ProfilePage clickProfileLink(String userName) {
        driver.findElement(By.xpath(String.format(profileLinkTest, userName))).click();
        return new ProfilePage(driver);
    }

    public Boolean checkExitLink() {
        return driver.findElements(exitLink).size() > 0;
    }

    public Boolean checkExistFilmName(String filmName) {
        return driver.findElements(exitLink).size() > 0;
    }

    public LoginPage clickExitLink() {
        driver.findElement(exitLink).click();
        return new LoginPage(driver);
    }

    public MainPage selectPaginationPageByNumber(String pageNumber) {
        WebElement page = driver.findElement(By.xpath(String.format("//div[@class='bot-navigation']//a[contains(text(), '%s')]", pageNumber)));
        if (page.isEnabled()) {
            page.click();
        }
        return this;
    }

    public List<String> selectAllFilmsFromPaginationPage() {
        List<String> nameFilms = new ArrayList<>();
        films = driver.findElements(allFilmsFromPage);
        for (WebElement film : films) {
            nameFilms.add(film.getText());
        }
        return nameFilms;
    }

    public boolean existMovieNameOnPage(String movieName) {
        List<String> nameFilms = new ArrayList<>();
        films = driver.findElements(allFilmsFromPage);
        for (WebElement film : films) {
            nameFilms.add(film.getText());
        }
        return nameFilms.contains(movieName);
    }

}
