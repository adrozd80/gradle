import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LoginPage {
    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

//    Page Factory
//    @FindBy(xpath = "//div[@class='loginin']//a[2]")
//    private List<WebElement> loginLinks;

//    @FindBy(xpath = "//div[@class='loginin']//a[2]")
//    private WebElement loginLink;

//    @FindBy(xpath = "//input[@name='login_name']")
//    private WebElement loginNameField;

//    @FindBy(xpath = "//input[@name='login_password']")
//    private WebElement passwordField;

//    @FindBy(xpath = "//div[@class='loginin']/following-sibling::div/form/button")
//    private WebElement logInButton;

//    @FindBy(xpath = "//h1[text()='Ошибка авторизации']")
//    private WebElement errorLogin;

//    @FindBy(xpath = "//div[@class='oformlenie']")
//    private WebElement errorLoginText;

//    Page Object
    private By loginLink = By.xpath("//div[@class='loginin']//a[2]");
    private By registrationLink = By.xpath("//a[@href='https://kinogo.by/index.php?do=register']");
    private By loginNameField = By.xpath("//input[@name='login_name']");
    private By passwordField = By.xpath("//input[@name='login_password']");
    private By logInButton = By.xpath("//div[@class='loginin']/following-sibling::div/form/button");
    private By errorLogin = By.xpath("//div[@class='oformlenie']/h1");

    public String invalidUser = "user";
    public String invalidPassword = "user";

    public Boolean checkLoginLink() {
        return driver.findElements(loginLink).size() > 0; // Page Object
//      return loginLinks.size() > 0; // Page Factory
    }

    public LoginPage clickEnterLink() {
        driver.findElement(loginLink).click(); // Page Object
//      loginLink.click(); // Page Factory
        return this;
    }

    public SignUpPage clickRegistrationLink() {
        driver.findElement(registrationLink).click();
        return new SignUpPage(driver);
    }

    public LoginPage fillLoginNameField(String loginName) {
        driver.findElement(loginNameField).sendKeys(loginName); // Page Object
//      loginNameField.sendKeys(loginName); // Page Factory
        return this;
    }

    public LoginPage fillPasswordField(String password) {
        driver.findElement(passwordField).sendKeys(password); // Page Object
//      passwordField.sendKeys(password); // Page Factory
        return this;
    }

    public MainPage clickLoginButton() {
//      WebDriverWait wait = new WebDriverWait(driver, 5);
//      wait.until(ExpectedConditions.visibilityOfElementLocated(logInButton));
        driver.findElement(logInButton).click(); // Page Object
//      logInButton.click(); // Page Factory
        return new MainPage(driver);
    }

    public Boolean checkErrorLoginTextExist() {
//      System.out.println(driver.findElement(errorLogin).getText()); // Page Object
//      System.out.println(errorLogin.getText()); // Page Factory
//      System.out.println(driver.findElement(errorLoginText).getText());
        return driver.findElement(errorLogin).getText().length() > 0; // Page Object
//      System.out.println(errorLoginText.getText()); // Page Factory
    }

    public MainPage loginToSite(String loginName, String password) {
        this.clickEnterLink();
        this.fillLoginNameField(loginName);
        this.fillPasswordField(password);
        this.clickLoginButton();
        return new MainPage(driver);
    }

    public void failLoginToSite(String loginName, String password) {
        this.clickEnterLink();
        this.fillLoginNameField(loginName);
        this.fillPasswordField(password);
        this.clickLoginButton();
    }

}
